﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FilmDb.Core.Models;

namespace FilmDb.DataAccess.interfaces
{
  public interface IFilmRepository
  {
    Task<IEnumerable<Film>> GetItemsByCriteria(String criteria, String field = "", String path = "moviedata");
    Task<IEnumerable<Film>> GetAllFilms(String path = "moviedata");
  }
}

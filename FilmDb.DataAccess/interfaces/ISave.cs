﻿using System;
using System.Threading.Tasks;

namespace FilmDb.DataAccess.interfaces
{
  public interface ISave
  {
    Task SaveItem<T>(T item, String path);
  }
}

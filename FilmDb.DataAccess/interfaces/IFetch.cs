﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FilmDb.DataAccess.interfaces
{
  public interface IFetch
  {
      Task<IEnumerable<T>> GetItems<T>(String criteria, String searchString);
      Task<T> GetAllItems<T>();
  }
}

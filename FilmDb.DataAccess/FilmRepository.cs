﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using FilmDb.Core.Models;
using FilmDb.DataAccess.interfaces;

namespace FilmDb.DataAccess
{
  public class FilmRepository: JsonRepository, IFilmRepository
  {
    public async Task<IEnumerable<Film>> GetItemsByCriteria(String criteria, String field = "",  String path = DefaultPath)
    {
      FullPath = path;
      if (!File.Exists(FullPath))
        throw new FileNotFoundException($"File with path: '{FullPath}' not found");

      var data = await GetItem<Film>();

      return new SearchFilter().FilteredBySearchCriteria(data, criteria, field);
    }

    public async Task<IEnumerable<Film>> GetAllFilms(String path = DefaultPath)
    {
      FullPath = path;
      if (!File.Exists(FullPath))
        throw new FileNotFoundException($"File with path: '{FullPath}' not found");

      var result = await GetItem<Film>();
      return result;
    }
  }
}

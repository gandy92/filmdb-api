﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using FilmDb.Core.Models;
using Newtonsoft.Json;

namespace FilmDb.DataAccess
{
	internal class SearchFilter
	{
		private const Char WildCardCharacter = '*';

		private String _searchCriteria;
    private String _field;
		private Boolean _startWithWildCard;
		private Boolean _endWithWildCard;
		private Regex _wildCardStartWordMatch;
		private Regex _wildCardEndWordMatch;
		private IList<Film> _results;


		private String SearchCriteria
		{
			get => _searchCriteria;
			set
			{
				_startWithWildCard = value.StartsWith(WildCardCharacter);
				_endWithWildCard = value.EndsWith(WildCardCharacter);
				_searchCriteria = value.Trim(WildCardCharacter);
			}
		}

    internal IEnumerable<Film> FilteredBySearchCriteria(IList<Film> data, string searchCriteria, String field = "")
		{
			if (searchCriteria.Equals(String.Empty))
				return null;

			SearchCriteria = searchCriteria;
      _field = field;

			_results = new List<Film>();

			SetRegexPatterns();

			for (var index = 0; index < data.Count; index++)
			{
				var list = data[index];
				Filter(list);
			}

			return _results;
		}

		private void SetRegexPatterns()
		{
			if (_startWithWildCard && _endWithWildCard)
				return;
			_wildCardStartWordMatch = new Regex($@"(?i)(.*?){SearchCriteria}(?i)");
			_wildCardEndWordMatch = new Regex($@"(?i){SearchCriteria}(.*?)(?i)");
		}

		private void Filter(Film film)
		{
			if (ExtractResults(film))
				_results.Add(film);
		}

		private Boolean ExtractResults(Film film)
    {
      var json = ExractField(film);

       if (json == null || !json.Any())
         return false;

       if (_startWithWildCard == _endWithWildCard)
         return SearchUsingCriteria(json);

       if (_startWithWildCard && SearchWithWildcardAtStart(json))
         return true;

       if (_endWithWildCard && SearchWithWildcardAtEnd(json))
         return true;

       return false;
		}

    private String[] ExractField(Film film)
    {
      switch (_field)
      {
        case "title": return new[] {film.Title};
        case "year": return new []{film.Year.ToString()};
        case "directors": return film.Details.Directors;
        case "rating": return new []{film.Details.Rating.ToString()};
        case "genres": return film.Details.Genres;
        case "rank": return new []{film.Details.Rank.ToString()};
        case "actors": return film.Details.Actors;
        case "length": return new []{film.Details.RunningTime.ToString()};
        case "": return new []{JsonConvert.SerializeObject(film)};
        default: return new String[]{};
      }
    }

    private Boolean SearchUsingCriteria(IReadOnlyList<String> filmAsJson) => filmAsJson[0].Contains(_searchCriteria, StringComparison.InvariantCultureIgnoreCase);

		private Boolean SearchWithWildcardAtStart(IReadOnlyList<String> filmAsJson)
		{
			for (var index = 0; index < filmAsJson.Count; index++)
			{
				if (_wildCardStartWordMatch.IsMatch(filmAsJson[index]))
					return true;
			}
			return false;
		}

		private Boolean SearchWithWildcardAtEnd(IReadOnlyList<String> filmAsJson)
    {
      for (int i = 0; i < filmAsJson.Count; i++)
      {
        if (_wildCardEndWordMatch.IsMatch(filmAsJson[i]))
            return true;
      }
      return false;
    }
	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FilmDb.DataAccess
{
  public abstract class JsonRepository
  {
    private protected const String DefaultPath = "moviedata";
    private const String BasePath = @"../";
    private String _path = "";

    private protected String FullPath
    {
      get => _path;
      set
      {
        if (string.IsNullOrEmpty(value))
          throw new ArgumentException("Value cannot be null or empty.", nameof(value));
        _path = $@"{BasePath}{value}.json";
      }
    }

    private protected async Task<IList<T>> GetItem<T>(String path = DefaultPath)
    {
      FullPath = path;
      if (!File.Exists(FullPath))
        throw new FileNotFoundException($"File with path: '{FullPath}' not found");
      await using var fs = new FileStream(FullPath, FileMode.Open);
      var r = await new StreamReader(fs).ReadToEndAsync().ConfigureAwait(false);

      return JsonConvert.DeserializeObject<List<T>>(r);
    }
  }
}

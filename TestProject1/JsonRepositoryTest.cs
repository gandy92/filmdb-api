﻿using System.Linq;
using FilmDb.Core.Models;
using Xunit;

namespace FilmDb.DataAccess.Tests
{
  public class JsonRepositoryTest
  {
    [Fact]
    public async void JsonRepo_Can_Get_All_Films()
    {
      var repo = new FilmRepository();

      var films = await repo.GetAllFilms();


      Assert.NotNull(films);
      Assert.True(films.Count() > 1);
      Assert.NotNull(films.First());
      Assert.True(films.First().GetType() == typeof(Film));
    }

    [Fact]
    public async void JsonRepo_Can_Get_A_Film()
    {
      var repo = new FilmRepository();
      var r = await repo.GetItemsByCriteria("Chris Hemsworth");

       Assert.True(r.Any());
       Assert.True(r.Count() == 9);
    }

    [Fact]
    public async void JsonRepo_Can_Get_A_Film_With_Wildcard_End()
    {
      var repo = new FilmRepository();
      var r = await repo.GetItemsByCriteria("Chris Hems*", "actors");

      Assert.True(r.Any());
      Assert.True(r.Count() == 9);
    }

    [Fact]
    public async void JsonRepo_Can_Get_A_Film_With_Wildcard_Start()
    {
      var repo = new FilmRepository();
      var r = await repo.GetItemsByCriteria("*ris Hemsworth", "actors");

      Assert.True(r.Any());
      Assert.True(r.Count() == 9);
    }
  }
}

# filmDb-api


This "still in dev" project is a simple api designed to retrive film Data from a
JSON file. 

The main feature and reason the api exists however, was to give me an excuse to 
try and make decent search feature. 

Currently the search feature accepts wildcards at the begining or end of a string
and also supoports a targeted search.

Next Steps - 

Look into JTOKEN comparisons on the search, this will avoid de-serialisation 
of the JSON and should speed things up.

Create more repositories (Control with DI) so I can test the search functionality
on other data sources.

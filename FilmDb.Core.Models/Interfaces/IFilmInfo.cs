﻿namespace FilmDb.Core.Models.Interfaces
{
  public interface IFilmInfo: IFilmDetail, IMediaDetail
  {
  }
}

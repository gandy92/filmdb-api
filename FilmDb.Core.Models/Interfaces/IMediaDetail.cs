﻿using System;

namespace FilmDb.Core.Models.Interfaces
{
  public interface IMediaDetail
  {
    DateTime ReleaseDate { get; }
    Double Rating { get; }
    String[] Genres { get; }
    String ImageUrl { get; }
    Int32 Rank { get; }
    Double RunningTime { get; }
  }
}

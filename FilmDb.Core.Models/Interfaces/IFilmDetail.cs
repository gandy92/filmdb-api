﻿using System;

namespace FilmDb.Core.Models.Interfaces
{
  public interface IFilmDetail
  {
    String[] Directors { get; }
    String Plot { get; }
    String[] Actors { get; }
  }
}
﻿using System;

namespace FilmDb.Core.Models.Interfaces
{
  public interface IMedia
  {
    String Title { get; }
    Int32 Year { get; }
    Details Details { get; }
  }
}

﻿using System;
using FilmDb.Core.Models.Interfaces;
using Newtonsoft.Json;

namespace FilmDb.Core.Models
{
  public class Details: IFilmInfo
  {
    [JsonProperty("directors")]
    public String[] Directors { get; set; }

    [JsonProperty("plot")]
    public String Plot { get; set; }

    [JsonProperty("actors")]
    public String[] Actors { get; set; }

    [JsonProperty("release_date")]
    public DateTime ReleaseDate { get; set; }

    [JsonProperty("rating")]
    public Double Rating { get; set; }

    [JsonProperty("genres")]
    public String[] Genres { get; set; }

    [JsonProperty("image_url")]
    public String ImageUrl { get; set; }

    [JsonProperty("rank")]
    public Int32 Rank { get; set; }

    [JsonProperty("running_time_secs")]
    public Double RunningTime { get; set; }
  }
}

﻿using System;
using FilmDb.Core.Models.Interfaces;
using Newtonsoft.Json;

namespace FilmDb.Core.Models
{
  public class Film: IMedia
  {
     [JsonProperty("title")]
     public String Title { get; set; }

     [JsonProperty("year")]
     public Int32 Year { get; set; }

     [JsonProperty("info")]
     public Details Details { get; set; }
  }
}

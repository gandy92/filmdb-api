using FilmDb.DataAccess;
using FilmDb.DataAccess.interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace FilmDb.Api
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
      services.AddCors(options =>
           {
             options.AddPolicy("custom",
               builder =>
               {
                 builder.WithOrigins("http://localhost:4200")
                   .AllowAnyHeader()
                   .AllowAnyMethod();
               });
           });
			services.AddControllers().AddNewtonsoftJson();
      services.LoadDependencies();

    }

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
      app.UseCors("custom");
			app.UseHttpsRedirection();

			app.UseRouting();

			app.UseAuthorization();

			app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

    }
	}

  public static class ServiceCollectionExtensions
  {
    public static IServiceCollection LoadDependencies(this IServiceCollection services)
    {
      services.AddScoped<IFilmRepository, FilmRepository>();
      return services;
    }
  }
}

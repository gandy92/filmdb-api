﻿using System;
using System.Threading.Tasks;
using FilmDb.DataAccess.interfaces;
using Microsoft.AspNetCore.Mvc;

namespace FilmDb.Api.Controllers
{
	[ApiController]
	[Route("films")]
	public class FilmController : ControllerBase
	{
	    private IFilmRepository _repository;

	    public FilmController(IFilmRepository repository)
		{
			_repository = repository;
		}

		[HttpGet]
		public async Task<IActionResult> GetAllFilms()
	    {
	      return Ok(await _repository.GetAllFilms());
	    }

	    [HttpGet("{searchCriteria}")]
	    public async Task<IActionResult> GetFilmsByCriteria(String searchCriteria)
	    {
	      return Ok(await _repository.GetItemsByCriteria(searchCriteria));
	    }

	    [HttpGet("{searchCriteria}/{field}")]
	    public async Task<IActionResult> GetFilmsByCriteria(String searchCriteria, String field)
	    {
	      return Ok(await _repository.GetItemsByCriteria(searchCriteria, field));
	    }
	}
}
